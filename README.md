# MovieDB

[![Carthage Compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
![Code Coverage](https://img.shields.io/badge/coverage-88%25-yellow.svg)
[![iOS Platform](https://img.shields.io/badge/platform-ios-lightgrey.svg)](https://img.shields.io/badge/platform-ios-lightgrey.svg)

## Requirments:

Your challenge is to produce a simple and well-engineered two-screen iOS app that displays a list of popular movies from The Movie DB (https://www.themoviedb.org)

Create an app that displays a list of popular movies in a list format (see page 2). It should be possible to tap on any movie, which should trigger a transition to the detail screen for that movie, where a larger version of the movie’s poster should be displayed (see page 3).

* For each movie in the list you should display its title, release_date, overview, vote_average and poster. It should be possible to view all ~17,500 popular movies in the app. Popular movies endpoint:  https://api.themoviedb.org/3/movie/popular?api_key=<<api_key>>&page=1 
* Movie poster URL prefix: 
http://image.tmdb.org/t/p/w500/<<poster_path>> 
* The release_date and vote_average should both be formatted to match how they are shown on page 2. If the vote average is at least 70%, the text should be green. If the vote average is at least 40%, the text should be orange. If the vote average is less than 40%, the text should be red.
* We want to see how you structure your code and will be assessing the performance and user experience of the app too. Have fun!

Bonus

* Implement favourites feature. The user should be able to tap on an icon (see the heart in the samples provided) to favourite a movie on the main and detail screens. Favourites should be persisted between app launches. 
* Add an animation, or implement the Lottie framework (https://github.com/airbnb/lottie-ios) with one of these loading animations: https://www.lottiefiles.com/search?q=loading  


## Notes

There are two view controllers, the intial listing controller and poster controller.

API protocol based services to fetch data and SDWebImage used to fetch and cache images for performance.

Favouriting has been added along with a Lottie animation loader in light  (cells) and dark modes (poster).

Given more time the APIMovie type would have been mapped to an Movie Realm object that can be used in line with a User to manage favouriting.

Unit testing has been done throughout with code coverage of 88%.

## System Requirements

- iOS 10.0+
- Xcode 10.0+
- Swift 4.0
- Command Line Tools 10.0+

## Documentation

Code has been document to a minumum of:

* Classes
* Functions
* Properties

Documentation can re-generated using jazz docs [Jazzy](https://github.com/realm/jazzy)

## Build

### Instructions

1. Checkout the application via Github or via a terminal.
2. Run `carthage update --platform iOS`
3. Open the application in Xcode and run.

## Author

Maninder Soor (http://manindersoor.com)
