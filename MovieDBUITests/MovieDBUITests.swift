//
//  MovieDBUITests.swift
//  MovieDBUITests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest

class MovieDBUITests: XCTestCase {

	///	Application
	let application = XCUIApplication()
	
	///	Timeout
	let timeout: TimeInterval = 320.0
	
	///	Exist predicate
	let existsPredicate = NSPredicate(format: "exists == 1")
	
	///	Doesnt exist predicate
	let doesntExistPredicate = NSPredicate(format: "exists == 0")
	
	
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        application.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testBasicLoading() {
		
		///	Initial state
		let activityVisible = expectation(for: existsPredicate,
										  evaluatedWith: application.otherElements.matching(identifier: "activity"),
										  handler: nil)
		let tableHidden = expectation(for: doesntExistPredicate,
										 evaluatedWith: application.tables.element(matching: XCUIElement.ElementType.table, identifier: "table") ,
										 handler: nil)
		
		/// Switched
		let activityHidden = expectation(for: doesntExistPredicate,
										  evaluatedWith: application.otherElements.matching(identifier: "activity"),
										  handler: nil)
		let tableVisible = expectation(for: existsPredicate,
									  evaluatedWith: application.tables.element(matching: XCUIElement.ElementType.table, identifier: "table") ,
									  handler: nil)
		
		XCTWaiter().wait(for: [activityVisible, tableHidden,
							   activityHidden, tableVisible],
						 timeout: timeout,
						 enforceOrder: true)
    }

}
