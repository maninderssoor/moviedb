//
//  UserViewModel.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	The user view model, temporarily, handles favourites. Ideally cached in a DB.
*/
public class UserViewModel {
	
	///	A movie
	internal let movie: Movie
	
	/**
		Initialiser
	*/
	init(movie: Movie) {
		self.movie = movie
	}
	
	// MARK: Actions
	
	/**
		If this movie is favourited
	*/
	public func isFavourite() -> Bool {
		return UserDefaults.standard.value(forKey: movie.title) != nil
	}
	
	/**
		Set a movie as favourites
	*/
	public func toggleFavourite() {
		let isOn = isFavourite()
		if isOn {
			UserDefaults.standard.removeObject(forKey: movie.title)
		} else {
			UserDefaults.standard.set("true", forKey: movie.title)
		}
	}
}
