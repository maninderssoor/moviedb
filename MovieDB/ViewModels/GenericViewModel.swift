//
//  GenericViewModel.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	The generic view model handles global functions for reusability
*/
public class GenericViewModel {
	
	// MARK: Private Variables
	
	///	Networking Manager
	private let networkingManager = NetworkManager()
	
	// MARK: Methods
	
	/**
		Load artwork
	*/
	public func loadArtwork(forURL url: String, completion: ((UIImage?) -> Void)? = nil) {
		let imageConfiguration = ContentImageViewConfiguration(urlString: url)
		networkingManager.fetchImage(with: imageConfiguration) { (image) in
			completion?(image)
		}
	}
	
}
