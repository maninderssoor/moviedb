//
//  ListingViewModel.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	The Movie Listing View Model handles
*/
public class ListingViewModel {
	
	///	A network manager
	internal let networkManager = NetworkManager()
	
    /// Movies.
	internal var movies = [Movie]()
    
    /// Limit reached.
    var limitReached: Bool = false
    
    /// Page. Could sit in DB when the data has been cached.
    internal var page: Int = 1
	
	// MARK: Methods
	
	/**
		Load the data
	*/
	public func loadData(completion: ((Bool) -> Void)? = nil) {
		guard limitReached == false else {
			print("Reached the limit. No need to network call anymore")
			completion?(false)
			return
		}
        let parameters = APIPopularMoviesParameter(page: self.page)
        networkManager.fetch(with: APIPopularMoviesService.self, and: parameters) { [weak self](results) in
			
			guard let self = self, let service = results.object as? APIPopularMoviesService, results.isSuccess else {
				print("Error parsing the results or no weak self")
				completion?(false)
				return
			}
			
            if service.movies.count == 0 {
                self.limitReached = true
				completion?(false)
			} else {
				self.page += 1
				let adapter = MovieAdapter(movies: service.movies)
				guard let dbMovies = adapter.convert() else {
					print("Error converting [APIMovie] into [Movie]")
					completion?(false)
					return
				}
				self.movies.append(contentsOf: dbMovies)
				completion?(true)
			}
		}
	}
	
	// MARK: UITableView Items
	
	/**
		Number of items.
     
        Return + 1 for the loading cell
	*/
	public func numberOfItems() -> Int {
        guard limitReached == false else {
            return movies.count
        }
		
		guard movies.count > 0 else {
			return 0
		}
        
        return movies.count + 1
	}
    
    /**
        Height for row at index path
    */
    public func heightForRow(atIndexPath indexPath: IndexPath) -> CGFloat {
        if item(atIndexPath: indexPath) == nil {
            return 60.0
        } else {
            return 140.0
        }
    }
	
	/**
		Item at index path
	*/
	public func item(atIndexPath indexPath: IndexPath) -> Movie? {
		guard indexPath.row < movies.count else {
			return nil
		}
		
		return movies[indexPath.row]
	}
}
