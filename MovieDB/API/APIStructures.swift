//
//  APIStructure.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	An API Progress object when the API call conforms to APIProtocol
*/
public struct APIProtocolCompletion {
	
	/// If the call was a success
	public let isSuccess: Bool
	
	/// Decoded JSON object being returned
	public let object: APIProtocol?
	
	///	The headers of the API Call
	public let headers: [AnyHashable : Any]?
	
	/// The full HTTP Response
	public let httpResponse: HTTPURLResponse?
	
	/// The HTTP Response Status Code
	public let httpResponseCode: Int?
	
	/// If there was an error, an error object
	public let error: Error?
	
	/**
		Basic Initialiser
	*/
	public init(isSuccess: Bool, object: APIProtocol? = nil, headers: [AnyHashable : Any]? = nil, httpResponse: HTTPURLResponse? = nil, httpResponseCode: Int? = nil, error: Error? = nil) {
		self.isSuccess = isSuccess
		self.object = object
		self.headers = headers
		self.httpResponse = httpResponse
		self.httpResponseCode = httpResponseCode
		self.error = error
	}
}

/**
	An Image Fetching Configuration for any Content
*/
public struct ContentImageViewConfiguration {
	
	///	A full URL.
	public let urlString: String
	
	/**
		Basic initialiser
	*/
	public init(urlString: String) {
		self.urlString = urlString
	}
}

