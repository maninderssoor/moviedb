//
//  GenericService.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Create a Popular Movies API Service
*/
public struct APIPopularMoviesService: APIProtocol {
	
	/// The name of this API call, for logging
	static public let title: String = "Popular Movies"
	
	/// The URL for this call
	static public let urlString: String = "\(HOST_URL)3/movie/popular?api_key=\(API_KEY)"
    
    /// Is fetching. For pagination
    static public var isFetching: Bool = false
    
    ///    Default parameters for the URL call
    public func parameters<E: APIProtocolParameters>(element: E) -> String? {
        return element.parameters()
    }

	///	An array of popular Movies object
	public let movies: [APIMovie]
	
	/**
		CodingKeys for the APISession
	*/
	enum CodingKeys: String, CodingKey {
		case movies = "results"
	}
}

/**
    Popular movies parameter
 */
struct APIPopularMoviesParameter: APIProtocolParameters {
    
    /// The page
    let page: Int
    
    func parameters() -> String? {
        return "&page=\(page)"
    }
    
}
