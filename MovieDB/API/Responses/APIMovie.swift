//
//  GenericResponse.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	APIMovie Type
*/
public struct APIMovie: Decodable {
	
	///	An Id for the movie
	public let id: Int
	
	///	The title
	public let title: String
	
	///	Overview of the movie
	public let overview: String
	
	///	The vote
	public let vote: Float
	
	///	Release Date "2019-01-03"
	public let releaseDate: String
	
	///	A slug for the image url
	public let imageSlug: String?
	
	/**
		CodingKeys
	*/
	enum CodingKeys: String, CodingKey {
		case id
		case title
		case overview
		case vote = "vote_average"
		case releaseDate = "release_date"
		case imageSlug = "poster_path"
	}
}
