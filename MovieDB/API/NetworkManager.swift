//
//  NetworkManager.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import SDWebImage
import UIKit

/**
	The network manager handles all API calls to the Last.fm API
*/
public class NetworkManager {
	
	/// A network sessions
	public let networkSession: URLSession
	
	///    Configuration
	private var configuration: URLSessionConfiguration = {
		let sessionConfiguration = URLSessionConfiguration.default
		sessionConfiguration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
		
		return sessionConfiguration
	}()
	
	/**
		Initalise with a configuration
	*/
	public init() {
		self.networkSession = URLSession(configuration: self.configuration, delegate: nil, delegateQueue: nil)
	}
	
	// MARK: Public Functions
	
	/**
		A generic API call to fetch from the API where objects should conform to decodable.
	*/
	public func fetch<E: APIProtocol, P: APIProtocolParameters>(with type: E.Type, and parameters: P? = nil, completion: ((APIProtocolCompletion) -> Void)? = nil) {
		
        guard type.isFetching == false else {
            print("Already fetching request for type \(String(describing: type.title))")
            return
        }
        type.isFetching = true
        
        var urlString = type.urlString
        if let params = parameters?.parameters() {
            urlString += "\(params)"
        }
        print("Fetching request for type \(String(describing: urlString))")
        
		guard let url = URL(string: urlString) else {
			print("Needed a valid URL")
            type.isFetching = false
			completion?(APIProtocolCompletion(isSuccess: false))
			return
		}
		
		let dataTask = networkSession.dataTask(with: url) { (data, _, error) in
			
			guard let data = data, error == nil else {
				DispatchQueue.main.async {
                    type.isFetching = false
					completion?(APIProtocolCompletion(isSuccess: false))
				}
				return
			}
			
			/// Convert to object
			do {
				
				print("[NetworkManager] Successfully fetched data for \(type)")
				let object = try JSONDecoder().decode(type, from: data)
				DispatchQueue.main.async {
                    type.isFetching = false
					let completionResponse = APIProtocolCompletion(isSuccess: true,
																   object: object)
					completion?(completionResponse)
				}
				
			} catch {
				print("[NetworkManager] Error in catch \(String(describing: error))")
				DispatchQueue.main.async {
                    type.isFetching = false
					let errorResponse = APIProtocolCompletion(isSuccess: false)
					completion?(errorResponse)
				}
				return
			}
			
		}
		
		dataTask.resume()
	}
	
	/// Fetchs image given a ContentImageViewConfiguration.  Only downloads the image if it can't be found in cache.
	/// If it is in cache it is returned immediately
	/// - Parameters:
	///     - configuration: configuration containing the url of the image to load
	///     - completion: the async callback called when the image download has completed.
	/// - Returns:
	///     - image if it is found in cache, nil if not
	public func fetchImage(with configuration: ContentImageViewConfiguration, completion: ((UIImage?) -> ())? = nil) {
		
		/// Setup URL
		print("[ContentViewModel] Loading image for URL \(configuration.urlString)")
		
		// if the image is found in cache return the image
		if let cachedImage = SDImageCache.shared.imageFromCache(forKey: configuration.urlString) {
			completion?(cachedImage)
			return
		}
		
		// if the image isn't found in cache then download it and return it via the completion callback.
		/// The fetch
		SDWebImageDownloader.shared.downloadImage(with: URL(string: configuration.urlString), options:  SDWebImageDownloaderOptions.init(rawValue: 0), progress: { (recievedSize, expectedSize, url) in
			
			let _ = Float(recievedSize) / Float(expectedSize)
			
		}) {(image, _, error, _) in
			
			/// Check there isn't an error
			guard error == nil else {
				print("[ContentViewModel] Couldn't load image with URL \(configuration.urlString), returned error \(String(describing: error))")
				completion?(nil)
				
				return
			}
			
			// cache the image
			SDImageCache.shared.store(image, forKey: configuration.urlString, completion: nil)
			completion?(image)
			
		}
	}
}
