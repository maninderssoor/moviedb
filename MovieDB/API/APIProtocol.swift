//
//  APIProtocol.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import SwiftyJSON

/**
	A protocol to handle API objects as part of the Networking Library.

	Defines base properties needed to make the API call.

	All but the AppConfiguration call should conform to Decodable for accesible data parsing.
*/
public protocol APIProtocol : Decodable {
	
	/// The name of this API call, for logging
	static var title: String { get }
	
	/// The URL for this API call
	static var urlString: String { get }
    
    /// If the request is already fetching
    static var isFetching: Bool { get set }
    
    /// Parameters for this URL calls
    func parameters<E: APIProtocolParameters>(element: E) -> String?
}

/**
    API Protocol Parameters
 */
public protocol APIProtocolParameters {
    
    ///    Returns parameters.
    ///    You can scale this by adding type/ method tp APIProtocol. For now I'll stick with URL encoded
    func parameters() -> String?
    
}
