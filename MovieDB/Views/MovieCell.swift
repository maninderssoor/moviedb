//
//  MovieCell.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit
import Lottie

/**
	MovieCell
*/
public class MovieCell: UITableViewCell {
	
	// MARK: Identifier
	static public let identifier = "MovieCell"
	
	// MARK: Internal Variables
	
	///	User view model
	internal var userViewModel: UserViewModel?
	
	///	Animation view
	internal var activity: AnimationView?
	
	// MARK: Outlets
	
	///	The image view
	@IBOutlet internal weak var imagePoster: UIImageView?
	
	///	The title
	@IBOutlet internal weak var labelTitle: UILabel?
	
	///	Release date label
	@IBOutlet internal weak var labelReleaseDate: UILabel?
	
	///	Overview label
	@IBOutlet internal weak var labelOverview: UILabel?
	
	///	Favourite button
	@IBOutlet weak var buttonFavourite: UIButton?
	
	///	Rating %
	@IBOutlet weak var labelRating: UILabel?
	
	
	// MARK: Setup
	
	/**
		Prepare for re-use
	*/
	public override func prepareForReuse() {
		super.prepareForReuse()
		
		if activity == nil {
			activity = AnimationView(animation: Animation.named("activity_dark"))
			activity?.contentMode = .scaleAspectFit
			activity?.frame = imagePoster?.frame ?? contentView.frame
			
			if let unwrappedActivityView = activity {
				contentView.addSubview(unwrappedActivityView)
			}
			activity?.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
		}
		activity?.alpha = 1.0
		
		imagePoster?.alpha = 0.0
		imagePoster?.image = nil
		labelTitle?.text = ""
		labelReleaseDate?.text = ""
		labelOverview?.text = ""
		labelRating?.text = ""
		buttonFavourite?.isSelected = false
	}
	
	/**
		Setup the cell
	*/
	public func setup(withMovie movie: Movie) {
		prepareForReuse()
		self.userViewModel = UserViewModel(movie: movie)
		
		labelTitle?.text = movie.title
		labelReleaseDate?.text = movie.releaseDate.readableReleaseDate()
		labelOverview?.text = movie.overview
		buttonFavourite?.isSelected = userViewModel?.isFavourite() ?? false
		labelRating?.attributedText = movie.vote.readableVote()
	}
	
	/**
		Load poster image
	*/
    public func setImage() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.activity?.alpha = 0.0
            self?.activity?.pause()
            
            self?.imagePoster?.alpha = 1.0
        }, completion: nil)
        
	}
	
	/**
		Favourite selection
	*/
	@IBAction public func toggleFavourite(sender: UIButton) {
		guard let userViewModel = userViewModel else {
			print("There isn't a user view model initialised")
			return
		}
		
		userViewModel.toggleFavourite()
		UIView.transition(with: sender, duration: 0.3, options: .transitionCrossDissolve, animations: {
			sender.isSelected = userViewModel.isFavourite()
		}, completion: nil)
	}
}
