//
//  LoadingCell.swift
//  MovieDB
//
//  Created by Maninder Soor on 11/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
    A loading cell for the paginated pre-fetching
 */
public class LoadingCell: UITableViewCell {
    
    // MARK: Identifier
    static public let identifier = "LoadingCell"
    
    // MARK: Outlets
    
    /// The activity loader
    @IBOutlet internal weak var activityLoader: UIActivityIndicatorView?
    
    // MARK: Setup
    
    /**
        Start animating
     */
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        activityLoader?.startAnimating()
    }
    
    /**
        Make sure it's still animating
     */
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        activityLoader?.startAnimating()
    }
}
