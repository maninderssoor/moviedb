//
//  String+Formatting.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

extension Date {
	
	/**
		Readable time format. Used for release date
	*/
	public func readableReleaseDate() -> String? {
		let readableFormatter = DateFormatter()
		readableFormatter.dateFormat = "MMMM d, YYYY"
		
		return readableFormatter.string(from: self)
	}
}

extension Float {
	
	/**
		A readable percentage vote
	*/
	public func readableVote() -> NSMutableAttributedString {
		let defaultFont = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
		let percentageFont = UIFont.systemFont(ofSize: 9.0, weight: .semibold)
		var fontColour = UIColor(red:0.25, green:0.65, blue:0.34, alpha:1.0)
		if self >= 7.0 {}
		else if self >= 4.0 {
			fontColour = UIColor(red:0.98, green:0.73, blue:0.18, alpha:1.0)
		} else {
			fontColour = UIColor(red:0.90, green:0.27, blue:0.24, alpha:1.0)
		}
		
		let formatted = String(format:"%.0f", self * 10)
		let percentage = "%"
		
		let attributes = [NSAttributedString.Key.font: defaultFont, NSAttributedString.Key.foregroundColor: fontColour]
		let attributedString = NSMutableAttributedString(string: "\(formatted)\(percentage)", attributes: attributes)
		
		if let rangeOfString : NSRange = (attributedString.string as? NSString)?.range(of: percentage){
			attributedString.addAttribute(NSAttributedString.Key.font, value: percentageFont, range: rangeOfString)
		}
		
		return attributedString
	}
}
