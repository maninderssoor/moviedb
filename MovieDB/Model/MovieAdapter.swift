//
//  MovieAdapter.swift
//  MovieDB
//
//  Created by Maninder Soor on 11/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

public class MovieAdapter {
	
	// MARK: Private Variables
	
	///	API Movies
	private let movies: [APIMovie]
	
	///	Date formatter
	private let formatter = DateFormatter()
	
	// MARK: Methods
	
	/**
		Initialiser
	*/
	init(movies: [APIMovie]) {
		self.movies = movies
		formatter.dateFormat = "YYYY-MM-dd"
	}
	
	/**
		Convert
	*/
	public func convert() -> [Movie]? {
		var dbMovies = [Movie]()
		
		for thisMovie in movies {
			
			guard let date = formatter.date(from: thisMovie.releaseDate) else {
				print("Error converting movie \(thisMovie.title)")
				continue
			}
			let imageURL = "\(IMAGE_HOST_URL)\(thisMovie.imageSlug ?? "")"
			
			let dbMovie = Movie(id: thisMovie.id,
								title: thisMovie.title,
								overview: thisMovie.overview,
								vote: thisMovie.vote,
								releaseDate: date,
								imageURL: imageURL)
			dbMovies.append(dbMovie)
		}
		
		return dbMovies.count > 0 ? dbMovies : nil
	}
}
