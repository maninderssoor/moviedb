//
//  Movie.swift
//  MovieDB
//
//  Created by Maninder Soor on 11/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	An example of a DB Movie type
*/
public struct Movie {
	
	///	An Id for the movie
	public let id: Int
	
	///	The title
	public let title: String
	
	///	Overview of the movie
	public let overview: String
	
	///	The vote
	public let vote: Float
	
	///	Release Date "2019-01-03"
	public let releaseDate: Date
	
	///	A slug for the image url
	public let imageURL: String
	
	/**
		Init
	*/
	init(id: Int, title: String, overview: String, vote: Float, releaseDate: Date, imageURL: String) {
		self.id = id
		self.title = title
		self.overview = overview
		self.vote = vote
		self.releaseDate = releaseDate
		self.imageURL = imageURL
	}
}
