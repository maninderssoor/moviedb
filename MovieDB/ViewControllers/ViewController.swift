//
//  ViewController.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import UIKit
import Lottie

/**
	The Listing View controller presents the top ranking movies in a UITableView
*/
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITableViewDataSourcePrefetching {
	
	// MARK: Identifiers
	
	///	Storyboard identifier
	static public let identifier = "ViewController"
	
	// MARK: Internal Variables
	
	///	View Model
	internal let viewModel = ListingViewModel()
    
    ///    Generic view model
    internal let genericViewModel = GenericViewModel()
	
	///	Activity animation view
	internal let activity = AnimationView()
	
	// MARK: UI
	
	///	The table view
	@IBOutlet internal weak var table: UITableView?
	
	// MARK: Lifecycle
	
	/**
		Setup the view
	*/
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupLoadingData(isLoading: true)
		setupTableView()
		setupLoadingView()
		loadData()
	}
	
	/**
		Reload table.
	
		Ideally you have a delegate callback from the detail view.
	*/
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		table?.reloadData()
	}
	
	// MARK: Setup
	
	/**
		Setup table view
	*/
	public func setupTableView() {
		table?.delegate = self
		table?.dataSource = self
        table?.prefetchDataSource = self
        
		table?.register(UINib(nibName: MovieCell.identifier, bundle: nil), forCellReuseIdentifier: MovieCell.identifier)
        table?.register(UINib(nibName: LoadingCell.identifier, bundle: nil), forCellReuseIdentifier: LoadingCell.identifier)
		table?.accessibilityIdentifier = "table"
	}
	
	/**
		Setup loading view
	*/
	public func setupLoadingView() {
		activity.animation = Animation.named("activity_dark")
		activity.accessibilityIdentifier = "activity"
		activity.contentMode = .scaleAspectFit
		activity.frame = view.frame
		view.addSubview(activity)
		activity.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
	}
	
	/**
		Animation for loading
	*/
	public func setupLoadingData(isLoading loading: Bool, completion: ((Bool) -> Void)? = nil) {
		UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: { [weak self] in
			
			self?.activity.alpha = loading ? 1.0 : 0.0
			self?.table?.alpha = loading ? 0.0 : 1.0
			
		}) { (isComplete) in
			completion?(isComplete)
		}
		
		if loading {
			activity.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
		} else {
			activity.pause()
		}
	}
	
	/**
		Load the data
	*/
	public func loadData(completion: ((Bool) -> Void)? = nil) {
		viewModel.loadData { [weak self] (isSuccess) in
			
			guard let self = self, isSuccess else {
				print("No weak self or error loading data!!!")
				return
			}
			
			self.table?.reloadData()
			self.setupLoadingData(isLoading: false)
		}
	}
	
	// MARK: UITableView Datasource
	
	/**
		Number of items
	*/
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.numberOfItems()
	}
	
	/**
		Cell for item at index path
	*/
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = cellForRow(atIndexPath: indexPath, forTableView: tableView) else {
            assertionFailure("Couldn't dequeue the correct cell")
            return UITableViewCell()
        }
        
		if let cell = cell as? MovieCell,
            let movie = viewModel.item(atIndexPath: indexPath) {
			cell.setup(withMovie: movie)
			cell.imagePoster?.sd_cancelCurrentImageLoad()
			cell.imagePoster?.sd_setImage(with: URL(string: movie.imageURL), completed: { (_, _, _, _) in
				cell.setImage()
			})
			
            return cell
		}
        else if let cell = cell as? LoadingCell {
            return cell
        } else {
            return cell
        }
	}
	
    /**
        Height setup
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.heightForRow(atIndexPath: indexPath)
    }
    
    /**
        Cell for row at index path
     */
    func cellForRow(atIndexPath indexPath: IndexPath, forTableView tableView: UITableView) -> UITableViewCell? {
        if viewModel.item(atIndexPath: indexPath) == nil {
            return tableView.dequeueReusableCell(withIdentifier: LoadingCell.identifier, for: indexPath)
        } else {
            return tableView.dequeueReusableCell(withIdentifier: MovieCell.identifier, for: indexPath)
        }
    }
	
	// MARK: UITableView Delegate
	
	/**
		Did select
	*/
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		guard let controller = UIStoryboard(name: PosterViewController.identifier, bundle: nil).instantiateInitialViewController() as? PosterViewController else {
			print("Couldn't initialise the detail view controller")
			return
		}
		
		guard let movie = viewModel.item(atIndexPath: indexPath) else {
			print("No movie at index path \(String(describing: indexPath))")
			return
		}
		
		controller.setMovie(withMovie: movie)
		navigationController?.pushViewController(controller, animated: true)
	}
    
    // MARK: Infinite Scroll
    
    /**
        If prefetching penultimate cell. Because the last one is the loading cell
     */
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let lastRow = tableView.numberOfRows(inSection: 0) - 1
        if indexPaths.filter({ $0.row == lastRow }).count > 0 {
            loadData { [weak self](_) in
                self?.table?.reloadData()
            }
        }
    }
}

