//
//  PosterViewController.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit
import Lottie

/**
	The Poster View controller presents a full screen view of a Movie
*/
class PosterViewController: UIViewController {
	
	// MARK: Identifiers
	
	///	Storyboard identifier
	static public let identifier = "PosterViewController"
	
	// MARK: Internal Variables
	
	///	Activity animation view
	internal let activity = AnimationView()
	
	///	A Movie
	internal var movie: Movie?
	
	///	A generic view model
	internal let genericViewModel = GenericViewModel()
	
	///	A user view model
	internal var userViewModel: UserViewModel?
	
	// MARK: UI
	
	///	The image view
	@IBOutlet internal weak var imageView: UIImageView?
	
	// MARK: Lifecycle
	
	/**
		Setup the view
	*/
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupLoadingData(isLoading: true)
		setupLoadingView()
		setupNavigationBar()
		loadArtwork()
	}
	
	// MARK: Setup
	
	/**
		Set the movie for this controller
	*/
	public func setMovie(withMovie movie: Movie) {
		self.movie = movie
		self.title = movie.title
		self.navigationItem.title = movie.title
		self.userViewModel = UserViewModel(movie: movie)
	}
	
	/**
		Setup loading view
	*/
	public func setupLoadingView() {
		activity.animation = Animation.named("activity")
		activity.contentMode = .scaleAspectFit
		activity.frame = view.frame
		view.addSubview(activity)
		activity.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
	}
	
	/**
		Setup Navigation Bar
	*/
	public func setupNavigationBar() {
		
		///	Back button
		let backButton = UIButton(type: .custom)
		backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 24)
		backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
		backButton.setImage(#imageLiteral(resourceName: "Arrow-Back"), for: .normal)
		backButton.setImage(#imageLiteral(resourceName: "Arrow-Back"), for: .highlighted)
		backButton.setImage(#imageLiteral(resourceName: "Arrow-Back"), for: .disabled)
		backButton.setTitle(nil, for: .normal)
		backButton.setTitle(nil, for: .highlighted)
		backButton.setTitle(nil, for: .disabled)
		
		let leftBarButton = UIBarButtonItem(customView: backButton)
		navigationItem.leftBarButtonItem = leftBarButton
		
		///	Favourite image
		guard let userViewModel = userViewModel else {
			print("You didn't initialise the User View Model")
			return
		}
		let favouriteButton = UIButton(type: .custom)
		let favouriteImage = userViewModel.isFavourite() ? #imageLiteral(resourceName: "Favourite_Selected") : #imageLiteral(resourceName: "Favourite")
		favouriteButton.frame = CGRect(x: 0, y: 0, width: 40, height: 24)
		favouriteButton.addTarget(self, action: #selector(toggleFavourite), for: .touchUpInside)
		favouriteButton.setImage(favouriteImage, for: .normal)
		favouriteButton.setImage(favouriteImage, for: .highlighted)
		favouriteButton.setImage(favouriteImage, for: .disabled)
		
		let favouriteView = UIBarButtonItem(customView: favouriteButton)
		navigationItem.rightBarButtonItem = favouriteView
	}
	
	/**
		Go back
	*/
	@IBAction public func goBack() {
		navigationController?.popViewController(animated: true)
	}
	
	/**
		Toggle favourite
	*/
	@IBAction public func toggleFavourite() {
		userViewModel?.toggleFavourite()
		setupNavigationBar()
	}
	
	/**
		Animation for loading
	*/
	public func setupLoadingData(isLoading loading: Bool, completion: ((Bool) -> Void)? = nil) {
		UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: { [weak self] in
			
			self?.activity.alpha = loading ? 1.0 : 0.0
			self?.imageView?.alpha = loading ? 0.0 : 1.0
			
		}) { (isComplete) in
			completion?(isComplete)
		}
		
		if loading {
			activity.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
		} else {
			activity.pause()
		}
	}
	
	/**
		Load the image
	*/
	public func loadArtwork(completion: ((Bool) -> Void)? = nil) {
		guard let imageURL = movie?.imageURL else {
			print("Error, a movie wasn't set")
			return
		}
		
		genericViewModel.loadArtwork(forURL: imageURL) { [weak self](image) in
			self?.imageView?.image = image
			
			UIView.animate(withDuration: 0.3, animations: {[weak self] in
				self?.activity.alpha = 0.0
				self?.activity.pause()
				self?.imageView?.alpha = 1.0
			}, completion: { (isComplete) in
				completion?(isComplete)
			})
		}
	}
}
