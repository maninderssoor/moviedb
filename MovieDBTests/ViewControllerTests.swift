//
//  ViewControllerTests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
import SDWebImage
@testable import MovieDB

/**
	View Controller tests
*/
class ViewControllerTests: MovieDBTests {
	
	///	Storyboard
	let storyboard = UIStoryboard(name: PosterViewController.identifier, bundle: Bundle(identifier: "com.manindersoor.MovieDB"))
	
	/**
		Test Setup of Poster View
	*/
	public func testSetup() {
		let asychronousExpectation = expectation(description: "testSetup")
		guard let controller = storyboard.instantiateInitialViewController() as? PosterViewController else {
			XCTFail("The controller wasn't initialised correctly")
			return
		}
		UserDefaults.standard.removeObject(forKey: movie.title)
		controller.setMovie(withMovie: dbMovie)
		controller.viewDidLoad()
		let _ = controller.view
		
		/// Setup
		controller.loadArtwork { [weak self] (isComplete) in
			XCTAssertNotNil(self, "Self should not be nil")
			XCTAssertNotNil(controller.imageView, "The image view should not be nil now")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Movie Cell Toggling
	*/
	public func testMovieCellToggle() {
		guard let xib = UINib(nibName: MovieCell.identifier, bundle: Bundle(identifier: "com.manindersoor.MovieDB")).instantiate(withOwner: nil, options: nil).first as? MovieCell else {
			XCTFail("Couldn't initalise the nib view")
			return
		}
		UserDefaults.standard.removeObject(forKey: movie.title)
		xib.setup(withMovie: dbMovie)
		xib.prepareForReuse()
		
		XCTAssertNil(UserDefaults.standard.value(forKey: movie.title), "There shouldn't be a favourite")
		
		xib.toggleFavourite(sender: UIButton())
		
		XCTAssertNotNil(UserDefaults.standard.value(forKey: movie.title), "This should be a favourite")
	}
}
