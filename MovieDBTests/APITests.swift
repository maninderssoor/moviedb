//
//  APITests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
import SDWebImage
@testable import MovieDB

/**
	API Tests, to check services are running correctly
*/
class APITests: MovieDBTests {
	
	/// A networking manager
	let networkingManager = NetworkManager()
	
	/**
		Test connections API Call
	*/
	public func testConnectionsAPICall() {
		let asychronousExpectation = expectation(description: "testConnectionsAPICall")
		
		let parameters = APIPopularMoviesParameter(page: 1)
		networkingManager.fetch(with: APIPopularMoviesService.self, and: parameters) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertNotNil(results.object, "The object returned should not be nil")
			
			guard let service = results.object as? APIPopularMoviesService else {
				XCTFail("The results.object wasn't a APIPopularMoviesService")
				asychronousExpectation.fulfill()
				return
			}
			
			let movies = service.movies
			XCTAssertGreaterThan(movies.count, 0, "There should have been some movies retruned")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test connections no url string API Call
	*/
	public func testNoURLConnectionsAPICall() {
		let asychronousExpectation = expectation(description: "testNoURLConnectionsAPICall")
		
		let emptyParameters = APITestParameters()
		networkingManager.fetch(with: APITestNoURL.self, and: emptyParameters) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test networking manager bad API
	*/
	public func testBadAPICall() {
		let asychronousExpectation = expectation(description: "testBadAPICall")
		
		let emptyParameters = APITestParameters()
		networkingManager.fetch(with: APITestBadURL.self, and: emptyParameters) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertFalse(results.isSuccess, "The call shouldn't have been a success")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Image URL test
	*/
	public func testImageURL() {
		let asychronousExpectation = expectation(description: "testImageURL")
		
		SDImageCache.shared.clearMemory()
		let emptyParameters = APITestParameters()
		networkingManager.fetch(with: APIPopularMoviesService.self, and: emptyParameters) { [weak self] (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertNotNil(results.object, "The object returned should not be nil")
			
			guard let service = results.object as? APIPopularMoviesService else {
				XCTFail("The results.object wasn't a APIPopularMoviesService")
				asychronousExpectation.fulfill()
				return
			}
			let adapter = MovieAdapter(movies: service.movies)
			let dbMovies = adapter.convert()
			XCTAssertNotNil(dbMovies, "There should be 1 move returned")
			
			guard let firstMovie = dbMovies?.first else {
				XCTFail("Couldn't fetch the first movie")
				asychronousExpectation.fulfill()
				return
			}
			let imageConfiguration = ContentImageViewConfiguration(urlString: firstMovie.imageURL)
			self?.networkingManager.fetchImage(with: imageConfiguration, completion: { (image) in
				XCTAssertNotNil(image, "The image returned should not be nil")
				asychronousExpectation.fulfill()
			})
			
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
}


/**
	No URL
*/
public struct APITestNoURL: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "NO URL"
	
	/// The URL for this call
	static public var urlString: String = ""
	
	///	If the call is fetching
	public static var isFetching: Bool = false
	
	/// Parameters
	public func parameters<E>(element: E) -> String? where E : APIProtocolParameters {
		return element.parameters()
	}
}


/**
	Fetch connection API
*/
public struct APITestBadURL: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "BAD URL"
	
	/// The URL for this call
	static public var urlString: String = "http://someurlthatwontwork.com"
	
	///	If the call is fetching
	public static var isFetching: Bool = false
	
	/// Parameters
	public func parameters<E>(element: E) -> String? where E : APIProtocolParameters {
		return element.parameters()
	}
}

/**
	Sample parameters
*/
public struct APITestParameters: APIProtocolParameters {
	
	/**
		Return
	*/
	public func parameters() -> String? {
		return nil
	}
}
