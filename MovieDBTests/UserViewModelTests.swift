//
//  UserViewModelTests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
import SDWebImage
@testable import MovieDB

/**
	View Model Tests
*/
class ViewModelTests: MovieDBTests {
	
	/**
		Test Favouriting
	*/
	public func testFavouriting() {
		UserDefaults.standard.removeObject(forKey: movie.title)
		let userViewModel = UserViewModel(movie: dbMovie)
		
		XCTAssertFalse(userViewModel.isFavourite(), "The movie should be false right now")
		
		userViewModel.toggleFavourite()
		
		XCTAssertTrue(userViewModel.isFavourite(), "The movie should be favourited now")
	}
	
	/**
		Test Listing view controller data
	*/
	public func testListingData() {
		let asychronousExpectation = expectation(description: "testListingData")
		let listingViewModel = ListingViewModel()
		listingViewModel.loadData { (isSuccess) in
			XCTAssertTrue(isSuccess, "The listing view model should be a success")
			
			listingViewModel.movies.removeAll()
			
			XCTAssertEqual(listingViewModel.numberOfItems(), 0, "There shouldn't be any items")
			XCTAssertNil(listingViewModel.item(atIndexPath: IndexPath(row: 0, section: 0)), "There shouldn't be any items")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Load on limit reached
	*/
	public func testLoadLimitReached() {
		let asychronousExpectation = expectation(description: "testLoadLimitReached")
		let listingViewModel = ListingViewModel()
		listingViewModel.limitReached = true
		
		listingViewModel.loadData { (isSuccess) in
			XCTAssertFalse(isSuccess, "The listing results should be unsuccessful")
			XCTAssertEqual(listingViewModel.numberOfItems(), listingViewModel.movies.count, "The number of items should be equal to the number of movies")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test height for valid cells
	*/
	public func testHeightForValidCells() {
		let listingViewModel = ListingViewModel()
		listingViewModel.movies = [dbMovie]
		
		let indexPath = IndexPath(row: 0, section: 0)
		XCTAssertEqual(listingViewModel.heightForRow(atIndexPath: indexPath), 140.0, "The height should be the expanded version")
	}
}
