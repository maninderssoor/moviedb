//
//  MovieDBTests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import MovieDB

class MovieDBTests: XCTestCase {

	///	Timeout for all asynchronous calls
	public let timeout = 120.0
	
	///	Mock Movie
	let movie = APIMovie(id: 166428,
						 title: "How to Train Your Dragon: The Hidden World",
						 overview: "As Hiccup fulfills his dream of creating a peaceful dragon utopia, Toothless’ discovery of an untamed, elusive mate draws the Night Fury away. When danger mounts at home and Hiccup’s reign as village chief is tested, both dragon and rider must make impossible decisions to save their kind.",
						 vote: 7.7,
						 releaseDate: "2019-01-03",
						 imageSlug: "/xvx4Yhf0DVH8G4LzNISpMfFBDy2.jpg")
	
	let dbMovie = Movie(id: 166428,
						title: "How to Train Your Dragon: The Hidden World",
						overview: "As Hiccup fulfills his dream of creating a peaceful dragon utopia, Toothless’ discovery of an untamed, elusive mate draws the Night Fury away. When danger mounts at home and Hiccup’s reign as village chief is tested, both dragon and rider must make impossible decisions to save their kind.",
						vote: 7.7,
						releaseDate: Date(),
						imageURL: "\(HOST_URL)/xvx4Yhf0DVH8G4LzNISpMfFBDy2.jpg")
	
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
